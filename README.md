# ファイナンシャルプランナーの予約システム

## セットアップ手順
1. このレポジトリをローカル環境にクローンし、プロジェクトのルートディレクトリに移動してください。
```
$ git clone https://github.com/keyskey/reservation.git
$ cd reservation
```
 
2. アプリを動かすのに必要なgemを次のコマンドでインストールします。
```
$ bundle install --without production --path vendor/bundle
```

3. データベースへのマイグレーションを実行します。
```
$ bundle exec rails db:migrate
```

4. テストを実行し、正常に動作しているか確認してください。
```
$ bundle exec rails test
```

5. 問題なければ次のコマンドでサーバーが立ち上がります。
```
$ bundle exec rails server
```

