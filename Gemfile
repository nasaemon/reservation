# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.5.1'

# Default gems
gem 'bootsnap', '>= 1.1.0', require: false
gem 'coffee-rails', '~> 4.2'
gem 'jbuilder', '~> 2.5'
gem 'mini_magick', '~> 4.8'
gem 'puma', '~> 3.11'
gem 'rails', '5.2.0'
gem 'sass-rails', '5.0.6'
gem 'turbolinks', '~> 5'
gem 'uglifier', '>= 1.3.0'

# Added gems
gem 'bcrypt', '~> 3.1.7'
gem 'bootstrap-sass', '3.3.7'
gem 'bootstrap-will_paginate', '1.0.0'
gem 'bootstrap3-datetimepicker-rails', '~> 4.17', '>= 4.17.47'
gem 'carrierwave', '~> 1.2', '>= 1.2.3'
gem 'faker', '~> 1.7.3'
gem 'will_paginate', '3.1.6'

group :development, :test do
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'pry-byebug'
  gem 'pry-rails'
  gem 'sqlite3', '1.3.13'
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'rubocop', '~> 0.59.1', require: false
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'web-console', '>= 3.3.0'
end

group :test do
  gem 'rspec', '~> 3.4'
end

group :production do
  gem 'mysql2', '0.5.2'
end

gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
